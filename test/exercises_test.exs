defmodule ExercisesTest do
  use ExUnit.Case
  doctest Exercises

  test "Greets the world" do
    data = [6, 1, 12, 5, 7]
    assert Exercises.ex1(data) == [18, 36, 21]
  end

  test "Сортировка яиц" do
    eggs = [
      {1, 2, 3},
      {2, 2, 3},
      {3, 3, 3},
      {1, 1, 4}
    ]
    assert Exercises.ex2(eggs) == {2, 3, 3}
  end

  test "Сумма красивых чисел" do
    assert Exercises.ex3(10000) == 24717105
  end
end
